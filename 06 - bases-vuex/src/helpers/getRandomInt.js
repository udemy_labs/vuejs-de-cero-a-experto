const getRandomInt = () => {
  return new Promise((resolve) => {
    const rnd = Math.floor(Math.random() * 100) + 1;
    setTimeout(() => {
      resolve(rnd);
    }, 2000);
  });
};

export default getRandomInt;
