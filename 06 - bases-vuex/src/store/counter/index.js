import state from "./state";
import { increment, incrementBy, setLoading } from "./mutations";
import * as actions from "./actions";
import { squareCount } from "./getters";

const counterStore = {
  namespaced: true,
  state,
  mutations: { increment, incrementBy, setLoading },
  actions,
  getters: { squareCount },
};

export default counterStore;
