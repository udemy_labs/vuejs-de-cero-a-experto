import getRandomInt from "@/helpers/getRandomInt";

export const incrementRandomInt = async (context) => {
  context.commit("setLoading");
  const rnd = await getRandomInt();
  context.commit("incrementBy", rnd);
  context.commit("setLoading");
};
