import { ref } from "vue";
import axios from "axios";

const useUsers = () => {
  const isLoading = ref(true);
  const users = ref([]);
  const currentPage = ref(1);
  const errorMessage = ref("");

  const getUsers = async (page = 1) => {
    if (page <= 0) page = 1;
    isLoading.value = true;
    const { data } = await axios.get("https://reqres.in/api/users", {
      params: {
        page: page,
      },
    });

    if (data.data.length > 0) {
      users.value = data.data;
      currentPage.value = page;
      isLoading.value = false;
      errorMessage.value = "";
    } else if (currentPage.value > 0) {
      errorMessage.value = "No hay mas usuarios";
    }
  };

  getUsers();
  const nextPage = () => getUsers(currentPage.value + 1);
  const previusPage = () => getUsers(currentPage.value - 1);
  const gotoInitPage = () => getUsers();
  return {
    users,
    currentPage,
    isLoading,
    errorMessage,
    //metodos
    nextPage,
    previusPage,
    gotoInitPage,
  };
};

export default useUsers;
