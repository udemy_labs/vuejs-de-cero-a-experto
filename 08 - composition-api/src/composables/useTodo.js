import { ref, computed } from "vue";
import { useStore } from "vuex";

const useTodo = () => {
  const store = useStore();
  const currentTab = ref("all");

  return {
    currentTab,

    //methods
    all: computed(() => store.getters["allTodos"]),
    completed: computed(() => store.getters["completedTodos"]),
    pending: computed(() => store.getters["pendingTodos"]),

    getTodosbytab: computed(() =>
      store.getters["getTodosByTab"](currentTab.value)
    ),

    toggleTodo: (id) => {
      store.commit("toggleTodo", id);
    },

    addTodo: (text) => {
      store.commit("createTodo", text);
    },
  };
};

export default useTodo;
