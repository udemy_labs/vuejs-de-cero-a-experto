console.log('Hola Mundo')

//Desestructuración de arreglos

const personajes = ['Goku', 'Vegeta', 'Trunks']

const goku = personajes[0]
const trunks = personajes[2]

console.log(`${goku} | ${trunks}`)

//ahora si desestrucutrado
const [g, v, t, gg = 'sin personaje'] = personajes
console.log(`${g} | ${t} | ${gg}`)


const returnArray = () => (
    ['ABC',123]
)

const [letters, numbers] = returnArray()
console.log(letters, numbers)
