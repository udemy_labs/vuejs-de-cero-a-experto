console.log("Hola Mundo");

//axios

import axios from "axios";

const apiKey = "q35RczJ80w0H19gyYxKKmA8Jo4NVumqG";

//fetch(`https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`

const giphyApi = axios.create({
  baseURL: "https://api.giphy.com/v1/gifs",
  params: {
    api_key: apiKey,
  },
});

giphyApi
  .get("/random")
  .then((resp) => {
    const img = document.createElement("img");
    img.src = resp.data.data.images.original.url;
    document.body.appendChild(img);
  })
  .catch((err) => {
    console.log(err);
  });

export default giphyApi;
