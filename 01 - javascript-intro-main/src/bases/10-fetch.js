console.log("Hola Mundo");

//FectAPI y Promesas

const apiKey = "q35RczJ80w0H19gyYxKKmA8Jo4NVumqG";

//https://api.giphy.com/v1/gifs/random?api_key=q35RczJ80w0H19gyYxKKmA8Jo4NVumqG

fetch(`https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`)
  .then((resp) => resp.json())
  .then(({ data }) => {
    const { url } = data.images.original;

    const img = document.createElement("img");
    img.src = url;
    document.body.appendChild(img);
  })
  .catch((err) => {
    console.log(err);
  });
