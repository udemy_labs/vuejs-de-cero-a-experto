console.log('Hola Mundo')

//Importaciones y Exportaciones


// import superHeroes from './data/heroes.js'
// console.log(superHeroes)

import heroes, { owners } from './data/heroes.js'
import { getHeroById, getHeroByOwner } from './data/heroes.js' //asi defino las funciones desde otro archivo y luego las importo

console.log(owners)
console.log(heroes)


// funciones de flecha, con find y filter
//getHeroById(id)
//getHeroeByOwner('owner')

// const getHeroById = (id) => {
//     return heroes.find( element => element.id === id)
// }

//forma aun mas resumida
// const getHeroById = (id) => heroes.find( element => element.id === id)
// const getHeroByOwner = (owner) => heroes.filter( element => element.owner===owner)

console.log(getHeroById(3))
console.log(getHeroByOwner('DC'))



    






