console.log("Hola Mundo");

//Async - Away, ejemplo rapido

const miPromesa = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("Promesa Cumplida");
    }, 1000);
  });
};

const medirTiempoAsync = async () => {
  try {
    console.log("Inicio");
    const respuesta = await miPromesa().then(console.log);
    console.log("Fin");
    return "Promesa tambien cumplida";
  } catch (error) {
    return error;
  }
};

medirTiempoAsync()
  .then((valor) => console.log(valor))
  .catch((err) => console.log(err));
