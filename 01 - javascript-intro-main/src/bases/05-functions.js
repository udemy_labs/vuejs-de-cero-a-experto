console.log('Hola')

// Funciones tradicionales
function saludar(nombre) {
    return `Hola Mundo ${nombre}`
}
const nombre = 'Tony'
console.log(saludar(nombre))


//funcion anonima, es mas segura ya que cuando se declara asi no se puede redefinir el nombre de la funcion como una variable, ejemplo saludar = true, destruiria la funcion anterior
//y la convertiria a una variable en este caso boleana con valor true. 
const saludar2 = function saludar(nombre) {
    return `Hola Mundo ${nombre}`
}
const nombre2 = 'Peter'
console.log(saludar2(nombre2))


//funciones de flecha
const saludar3 = (nombre) => `Hola Mundo ${nombre}`
const nombre3 = 'Desdemona'
console.log(saludar3(nombre3))

const getUSer = () => {
    return {
        uid: 'ABC123',
        username:'Tony Danza'
    }
}

// Otra forma mas comnprimida de la funcion anterior, en lo personal me gusta mas la primera forma para devolver un objeto
const getUSer2 = () => ({
        uid: 'ABC123',
        username:'Tony Danza'
    })
   

console.log(getUSer())
console.log(getUSer2())

const heroes = [
    {
        id: 1,
        name: 'batman'
    },
    {
        id: 2,
        name: 'superman'
    },
    {
        id: 3,
        name: 'aquaman'
    },
]


const existe = heroes.some((element) => {
    return element.id===1
})

const existe2 = heroes.find((element) => {
    return element.id===3
})

//forma desestrucutrada
const {name} = heroes.find((element) => {
    return element.id===3
})

console.log(existe)
console.log(existe2)
console.log(name)

