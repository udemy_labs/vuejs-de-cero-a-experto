console.log('Hola Mundo')

//Promesas

console.log('Inicio')

new Promise( (resolve, reject) => {
    console.log('Cuerpo de la Promesa')
    // resolve('Promesa Resuelta')
    reject('Sucedio un Error')

}).then(msg => console.log(msg))
    .catch(err => console.log(err))

console.log('Fin')

//promesas con argumentos
import {getHeroById} from './data/heroes'

const getHerobyIdAsync = (id) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const hero = getHeroById(id);
            if (!hero) {
                reject('heroe no existe');
            }
            resolve(hero);
        }, 1000);
    });
}

console.log(getHerobyIdAsync(2).then(msg => console.log(msg.name)).catch(err => console.log(err)));





    






