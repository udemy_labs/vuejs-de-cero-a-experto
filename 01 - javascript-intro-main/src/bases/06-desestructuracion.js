console.log('Hola Mundo')

//Desestructuración de objetos

const person = {
    name: 'Tony',
    age: 45,
    codeName: 'Ironman',
    power:'Money$$$$'
}

console.log(person.name)
console.log(person.age)
console.log(person.codeName)

const { name, age, codeName, power = 'Powerless' } = person
console.log(name)
console.log(age)
console.log(codeName)
console.log(power)

const newHeroe = ( person_var ) => {
    return {
        id: 123568,
        name: person_var.name,
        age: person_var.age,
        codename: person_var.codeName,
        power: person_var.power
    }
}

console.log(newHeroe(person))

const newHeroe2 = ({ name, age, codeName, power = 'Powerless' }) => {
    return {
        id: 123568,
        name: name,
        age: age,
        codename: codeName,
        power: power
    }
}
console.log(newHeroe2(person))

const newHeroe3 = (person) => {
    const { name, age, codeName, power = 'Powerless' } = person //->esta es una forma

    return {
        id: 123568,
        name: name,
        age: age,
        codename: codeName,
        power: power
    }
}

console.log(newHeroe3(person))

const newHeroe4 = ({ name, age, codeName, power = 'Powerless' }) => (
    {
        id: 123568,
        name: name,
        age: age,
        codename: codeName,
        power: power
    }
)
console.log(newHeroe4(person))



