const arreglo=new Array(10) //forma poco usada para declarar un arreglo
const arreglo2=[] //forma mas tradicional de declarar un arreglo

console.log(arreglo)
console.log(arreglo2)

const arreglo3 = [1,2,3,4] //recordemos que los arrglos comienzan en la posicion 0
arreglo3.push(5)

const arreglo4 = arreglo3.slice() //copia el arreglo por valor no por referencia
//const arreglo4 = [...arreglo3] //copia el arreglo por valor no por referencia usado el operado spread
arreglo4.push(6)

const arreglo5 = arreglo4.map((num)=>{return num*2})


console.log(`Aqui despliego todos los valores del arreglo 3: ${arreglo3}`)
console.log(`Esta es el 4to valor del arreglo 3: ${arreglo3[3]}`)
console.log(arreglo4)

console.log(arreglo5)