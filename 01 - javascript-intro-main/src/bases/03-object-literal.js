

const persona = {
    nombre:'Tony',
    apellido:'Stark',
    edad:45,
    direccion:{
        ciudad:'New York',
        zip: 569823,
        lat: 14.32569,
        long:85.74582
    }
}

const persona2 = {...persona} //copia el objeto por valor no por referencia spread operator
persona2.nombre='Peter'

console.log(persona)
console.log(persona2)