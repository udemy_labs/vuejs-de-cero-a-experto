console.log("Hola Mundo");

//ternarios y null check
let fn;
let ln = "Rodriguez";

console.log(`${fn || "Sin Nombre"} ${ln || "Sin Apellido"}`);

const isActive = true;

//let message = "";

// if (isActive) {
//   message = "Activo";
// } else {
//   message = "Inactivo";
// }

const message = isActive === true ? "Activo" : "Inactivo";

console.log(message);
