const heroes = [
    {
        id: 1,
        name: 'Batman',
        owner: 'DC'
    },
    {
        id: 2,
        name: 'Spiderman',
        owner: 'Marvel'
    },
    {
        id: 3,
        name: 'Superman',
        owner: 'DC'
    },
    {
        id: 4,
        name: 'Flash',
        owner: 'DC'
    },
    {
        id: 5,
        name: 'Wolverine',
        owner: 'Marvel'
    },
];


const getHeroById = (id) => heroes.find(element => element.id === id);
const getHeroByOwner = (owner) => heroes.filter(element => element.owner === owner);


export const owners = ['DC', 'Marvel'];
export { getHeroById, getHeroByOwner };
export default heroes;


