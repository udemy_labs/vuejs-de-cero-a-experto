const isAuthenticatedGuard = async (to, from, next) => {
  //console.log({ to, from, next });
  return new Promise(() => {
    const random = Math.random() * 100;
    if (random > 50) {
      console.log(`Paso isAuthenticatedGuard ${random}`);
      next();
    } else {
      console.log(`Bloqueado isAuthenticatedGuard ${random}`);
      next({ name: "pokemon" });
    }
  });
};

export default isAuthenticatedGuard;
