// import { resolve } from "core-js/fn/promise";
import { createRouter, createWebHashHistory } from "vue-router";
import isAuthenticatedGuard from "./auth-guard";

// import ListPage from "../modules/pokemon/pages/ListPage.vue";
// import AboutPage from "../modules/pokemon/pages/AboutPage.vue";
// import PokemonPage from "../modules/pokemon/pages/PokemonPage.vue";
// import NoPageFound from "../modules/shared/pages/NoPageFound.vue";

const routes = [
  {
    path: "/",
    redirect: "/pokemon",
  },

  {
    path: "/pokemon",
    name: "pokemon",
    component: () =>
      import(
        /* webpackChunkName:"PokemonLayout" */ "@/modules/pokemon/layouts/PokemonLayout.vue"
      ),
    children: [
      {
        path: "list",
        name: "pokemon-home",
        component: () =>
          import(
            /* webpackChunkName:"ListPage" */ "@/modules/pokemon/pages/ListPage.vue"
          ),
      },

      {
        path: "about",
        name: "pokemon-about",
        component: () =>
          import(
            /* webpackChunkName:"AboutPage" */ "@/modules/pokemon/pages/AboutPage.vue"
          ),
      },
      {
        path: "single/:id",
        name: "pokemon-id",
        component: () =>
          import(
            /* webpackChunkName:"PokemonPage" */ "@/modules/pokemon/pages/PokemonPage.vue"
          ),
        props: (route) => {
          const { id } = route.params;
          return isNaN(Number(id))
            ? { redirect: { name: "404" } }
            : { id: Number(id) };
        },
      },
      {
        path: "",
        redirect: { name: "pokemon" },
      },
    ],
  },
  // DBZ Routes
  {
    path: "/dbz",
    name: "dbz",
    beforeEnter: [isAuthenticatedGuard],
    component: () =>
      import(
        /* webpackChunkName:"DragonBallLayout" */ "@/modules/dbz/layouts/DragonBallLayout.vue"
      ),
    children: [
      {
        path: "characters",
        name: "dbz-characters",
        component: () =>
          import(
            /* webpackChunkName:"Characters" */ "@/modules/dbz/pages/Characters.vue"
          ),
      },

      {
        path: "about",
        name: "dbz-about",
        component: () =>
          import(
            /* webpackChunkName:"About" */ "@/modules/dbz/pages/About.vue"
          ),
      },

      {
        path: "",
        redirect: { name: "dbz-characters" },
      },
    ],
  },
  {
    path: "/:pathMatch(.*)*",
    component: () =>
      import(
        /* webpackChunkName:"NoPageFound" */ "../modules/shared/pages/NoPageFound.vue"
      ),
    name: "404",
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

// Guard Global - Sincrono
// router.beforeEach((to, from, next) => {
//   console.log({ to, from, next });
//   const random = Math.random() * 100;
//   if (random > 50) {
//     console.log(`Paso ${random}`);
//     next();
//   } else {
//     console.log(`Bloqueado ${random}`);
//     next({ name: "pokemon" });
//   }
// });

//Guard - Asincrono
// const canAccess = () => {
//   return new Promise((resolve) => {
//     const random = Math.random() * 100;
//     if (random > 50) {
//       console.log(`Paso - CanAccess ${random}`);
//       resolve(true);
//     } else {
//       console.log(`Bloqueado ${random}`);
//       resolve(false);
//     }
//   });
// };

// router.beforeEach(async (to, from, next) => {
//   const authorized = await canAccess();
//   if (authorized) {
//     next();
//   } else {
//     next({ name: "pokemon" });
//   }
// });

export default router;
