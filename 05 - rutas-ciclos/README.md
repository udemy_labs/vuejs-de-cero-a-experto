# rutas-ciclos

5to Ejercicio del Curso de Cero a Experto por Fernando Herrera
Seccion 9 : Vue Router, Ciclo de Vida de los Componentes - Options API

Recursos Externos Utilizados:

Fuente: Udemy/DevTalles

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
