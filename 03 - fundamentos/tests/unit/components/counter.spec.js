import { shallowMount, mount } from "@vue/test-utils";
import Counter from "@/components/Counter.vue";

describe("Counter Component", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Counter);
  });

  test("debe hacer match con el snapshot", () => {
    //const wrapper = shallowMount(Counter);
    expect(wrapper.html()).toMatchSnapshot();
  });

  test('h2 debe el tener el valor por defecto "Counter"', () => {
    //const wrapper = shallowMount(Counter);

    expect(wrapper.find("h2").exists()).toBeTruthy();
    const h2 = wrapper.find("h2");
    expect(h2.text()).toBe("Counter");
  });

  test("El valor por defecto debe ser 5 en P", () => {
    //wrapper
    //const wrapper = shallowMount(Counter);
    //p tags
    const p = wrapper.findAll("p");

    // expect segundo p = 5
    expect(p[1].text()).toBe("5");
  });

  // test("debe incrementar y decrementar el valor de counter", async () => {
  //   //const wrapper = shallowMount(Counter);

  //   const increaseBtn = wrapper.find("button");
  //   await increaseBtn.trigger("click");
  //   let p = wrapper.findAll("p");
  //   expect(p[1].text()).toBe("6");

  //   const decreaseBtn = wrapper.findAll("button")[1];

  //   await decreaseBtn.trigger("click");
  //   await decreaseBtn.trigger("click");

  //   p = wrapper.findAll("p");
  //   expect(p[1].text()).toBe("4");
  // });

  //Refactorizado para simplificar el test anterior
  test("debe incrementar y decrementar el valor de counter", async () => {
    //const wrapper = shallowMount(Counter);

    const [increaseBtn, decreaseBtn] = wrapper.findAll("button");

    await increaseBtn.trigger("click"); //Valor inicial 5, 5 + 1= 6
    await decreaseBtn.trigger("click"); //Valor actual 6, 6 - 1 = 5
    await decreaseBtn.trigger("click"); //Valor actual 5, 5 - 1 = 4

    let p = wrapper.findAll("p");
    expect(p[1].text()).toBe("4");
  });

  test("Debe establecer el valor por defecto", () => {
    const { counterStart } = wrapper.props();
    const value = wrapper.findAll("p")[1].text();
    console.log(value);
    console.log(counterStart);
    expect(Number(value)).toBe(counterStart);
  });

  test("Debe mostrar la prop title", () => {
    const testTitle = "Hola Mundo!!";
    const wrapper = shallowMount(Counter, {
      props: {
        title: testTitle,
      },
    });

    expect(wrapper.find("h2").text()).toBe(testTitle);
  });
});
