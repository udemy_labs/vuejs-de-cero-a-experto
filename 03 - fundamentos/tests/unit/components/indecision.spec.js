import IndecisionVue from "@/components/Indecision.vue";
import { shallowMount, mount } from "@vue/test-utils";

describe("Indecision Component", () => {
  let wrapper;
  let consoleSpy;
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () =>
        Promise.resolve({
          answer: "yes",
          forced: false,
          image: "https://yesno.wtf/assets/yes/2.gif",
        }),
    })
  );

  beforeEach(() => {
    wrapper = shallowMount(IndecisionVue);
    consoleSpy = jest.spyOn(console, "log");
    jest.clearAllMocks();
  });

  test("debe hacer match con el snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  test("Escribir en el input no debe disparar nada (console.log)", async () => {
    const getAnswerSpy = jest.spyOn(wrapper.vm, "getAnswer");
    const input = wrapper.find("input");
    await input.setValue("Hola Mundo");
    expect(consoleSpy).toHaveBeenCalled(); //llamado al menos una vez
    expect(consoleSpy).toHaveBeenCalledTimes(1); //llamado la cantidad de veces que indiquemos en el parametro
    expect(consoleSpy).toHaveBeenCalledWith("Hola Mundo"); //evaluar el contenido

    //para evaluar que el metodo no se haya llamado/ejecutado ninguna vez (0), puede hacerse de dos formas,
    //A:
    expect(getAnswerSpy).toHaveReturnedTimes(0);
    //B:
    expect(getAnswerSpy).not.toHaveBeenCalled();
  });

  test("Al escribir el '?' se debe disparar el getAnswer", async () => {
    const getAnswerSpy = jest.spyOn(wrapper.vm, "getAnswer");
    const input = wrapper.find("input");
    await input.setValue("Esto es una pregunta?");
    expect(getAnswerSpy).toHaveReturnedTimes(1);
  });

  test("Pruebas en getAnswer", async () => {
    await wrapper.vm.getAnswer();
    // console.log(wrapper.vm.image);
    // console.log(wrapper.vm.answer);
    expect(wrapper.vm.answer).toBe("Si!");
    expect(wrapper.vm.image).toBe("https://yesno.wtf/assets/yes/2.gif");
  });

  test("Pruebas en el get Answer - Fallo en el API", async () => {
    fetch.mockImplementationOnce(() => Promise.reject("API is down"));
    await wrapper.vm.getAnswer();
    expect(wrapper.vm.image).toBe("https://via.placeholder.com/250"); //esta es la imagen que estableci por defecto, no nuleo la imagen
    expect(wrapper.vm.answer).toBe("No se pudo cargar el API");
  });
});
