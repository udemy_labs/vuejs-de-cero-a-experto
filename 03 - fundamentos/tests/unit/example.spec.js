// import { shallowMount } from '@vue/test-utils'
// import HelloWorld from '@/components/HelloWorld.vue'

// describe('HelloWorld.vue', () => {
//   it('renders props.msg when passed', () => {
//     const msg = 'new message'
//     const wrapper = shallowMount(HelloWorld, {
//       props: { msg }
//     })
//     expect(wrapper.text()).toMatch(msg)
//   })
// })

describe("Example Componenent", () => {
  test("Debe ser mayor a 10", () => {
    ///AAA ->

    //Arreglar
    let value = 9;

    //Estimulo
    value = value + 2;

    //Observar el Resultado

    // metodo enpirico
    // if (value > 10) {
    //   //TODO: todo esta bien
    // } else {
    //   throw `${value} no es mayor a 10`;
    // }

    // metodo jest -> que es lo que deberia usar ya que fue el motor de test units que instale con npm
    expect(value).toBeGreaterThan(10);
  });
});
