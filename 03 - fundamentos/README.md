# fundamentos de vuejs

3er Ejercicio del Curso de Cero a Experto por Fernando Herrera
Seccion 4, 5, y 6 : Counter Component, Indecision Component. Pruebas Unitarias respectivamente

Recursos Externos Utilizados:
https://jestjs.io/docs/api
https://yesno.wtf/

Fuente: Udemy/DevTalles

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```
