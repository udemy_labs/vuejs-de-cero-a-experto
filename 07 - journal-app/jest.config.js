module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  transformIgnorePatterns: ["/node_modules/(?!(axios)/).*"],
};

//Esta linea en el config es cuando toda la app esta toda
//construida con compisition api, si hay options api
//las pruebas del router fallarian, solo se hace el config global
//si la app es toda de composition api
//setupFilesAfterEnv: ["<rootDir>/jest.setup.js"],
