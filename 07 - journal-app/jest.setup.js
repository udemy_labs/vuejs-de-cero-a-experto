// This file is automatically loaded by Jest before each test file.
// It's useful for setting up the testing environment.
// https://jestjs.io/docs/configuration#setupfiles-array
// solo debe usarse asi cuando toda la app use composition api

import {
  VueRouterMock,
  createRouterMock,
  injectRouterMock,
} from "vue-router-mock";
import { config } from "@vue/test-utils";

// create one router per test file
const router = createRouterMock();
beforeEach(() => {
  router.reset(); // reset the router state
  injectRouterMock(router);
});

// Add properties to the wrapper
config.plugins.VueWrapper.install(VueRouterMock);
