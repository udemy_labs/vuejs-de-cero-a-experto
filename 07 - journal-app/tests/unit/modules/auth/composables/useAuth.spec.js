import useAuth from "@/modules/auth/composables/useAuth";

const mockStore = {
  dispatch: jest.fn(),
  getters: {
    "auth/currentAuthState": "authenticated",
    "auth/username": "test",
  },
};

jest.mock("vuex", () => ({
  useStore: () => mockStore,
}));

describe("useAuth - Composable", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("useAuth - creacion del usuario debe ser exitosa", async () => {
    const { createUser } = useAuth();

    const newUser = {
      name: "test",
      email: "test@test.com",
      password: "123456",
    };

    mockStore.dispatch.mockResolvedValueOnce({ ok: true });
    const resp = await createUser(newUser);
    expect(mockStore.dispatch).toHaveBeenCalledWith("auth/createUser", newUser);

    expect(resp).toEqual({ ok: true });
  });

  test("useAuth - creacion debe ser fallida", async () => {
    const { createUser } = useAuth();

    const newUser = {
      name: "test",
      email: "test@test.com",
      password: "123456",
    };

    mockStore.dispatch.mockResolvedValueOnce({
      ok: false,
      message: "EMAIL_EXISTS",
    });

    const resp = await createUser(newUser);
    expect(mockStore.dispatch).toHaveBeenCalledWith("auth/createUser", newUser);

    expect(resp).toEqual({
      ok: false,
      message: "EMAIL_EXISTS",
    });
  });

  test("useAuth - inicio de sesión debe ser exitoso", async () => {
    const { signInUser } = useAuth();

    const loginForm = {
      email: "test@test.com",
      password: "123456",
    };

    mockStore.dispatch.mockResolvedValue({ ok: true });

    const resp = await signInUser(loginForm);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      "auth/signInUser",
      loginForm
    );
    expect(resp).toEqual({ ok: true });
  });

  test("useAuth - inicio de sesión debe ser fallido", async () => {
    const { signInUser } = useAuth();

    const loginForm = {
      email: "test@test.com",
      password: "123456",
    };

    mockStore.dispatch.mockResolvedValue({
      ok: false,
      message: "INVALID_PASSWORD",
    });

    const resp = await signInUser(loginForm);
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      "auth/signInUser",
      loginForm
    );
    expect(resp).toEqual({ ok: false, message: "INVALID_PASSWORD" });
  });

  test("useAuth - checkear el status", async () => {
    const { checkAuthState } = useAuth();

    mockStore.dispatch.mockResolvedValue({
      ok: true,
    });

    const resp = await checkAuthState();
    expect(mockStore.dispatch).toHaveBeenCalledWith("auth/checkAuthState");
    expect(resp).toEqual({ ok: true });
  });

  test("useAuth - logout", async () => {
    const { logoutUser } = useAuth();

    await logoutUser();
    expect(mockStore.dispatch).toHaveBeenCalledWith("auth/logoutUser");
    expect(mockStore.dispatch).toHaveBeenCalledWith("journal/clearEntries");
  });

  test("useAuth - authStatus and username", () => {
    const { authStatus, username } = useAuth();

    expect(authStatus.value).toBe("authenticated");
    expect(username.value).toBe("test");
  });
});
