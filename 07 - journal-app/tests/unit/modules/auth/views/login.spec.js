import { shallowMount } from "@vue/test-utils";
import Login from "@/modules/auth/views/LoginView.vue";
import createVuexStore from "../../../mock-data/mock-store";

//Esto es para que las pruebas del router no fallen con el composition api
import {
  VueRouterMock,
  createRouterMock,
  injectRouterMock,
} from "vue-router-mock";
import { config } from "@vue/test-utils";

// create one router per test file
const router = createRouterMock();
beforeEach(() => {
  router.reset(); // reset the router state
  injectRouterMock(router);
});

// Add properties to the wrapper
config.plugins.VueWrapper.install(VueRouterMock);
//Fin de la configuracion para que las pruebas del router no fallen con el composition api

//i need to mock sweetalrt2
import Swal from "sweetalert2";
jest.mock("sweetalert2", () => ({
  fire: jest.fn(),
  showLoading: jest.fn(),
  close: jest.fn(),
}));

describe("Pruebas en el Login View", () => {
  const store = createVuexStore({
    user: null,
    status: "no-authenticated",
    idToken: null,
    refreshToken: null,
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("debe hacer match con el snapshot", () => {
    const wrapper = shallowMount(Login, {
      global: {
        plugins: [store],
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  test("credenciales vacias, no se dispara el login", async () => {
    const wrapper = shallowMount(Login, {
      global: {
        plugins: [store],
      },
    });

    store.dispatch = jest.fn();
    store.dispatch.mockReturnValue({
      ok: false,
      message: "Error en Credenciales",
    });

    await wrapper.find("form").trigger("submit.prevent");

    expect(store.dispatch).toHaveBeenCalledWith("auth/signInUser", {
      email: "",
      password: "",
    });

    expect(Swal.fire).toHaveBeenCalledWith({
      icon: "error",
      position: "top-end",
      showConfirmButton: false,
      timer: 1500,
      title: "Error en Credenciales",
    });

    expect(store.state.auth).toEqual({
      user: null,
      status: "no-authenticated",
      idToken: null,
      refreshToken: null,
    });
  });

  test("debe disparar el login, y hacer push a la ruta no-entry", async () => {
    const wrapper = shallowMount(Login, {
      global: {
        plugins: [store],
      },
    });

    store.dispatch = jest.fn();
    store.dispatch.mockReturnValue({ ok: true });

    const [email, password] = wrapper.findAll("input");
    await email.setValue("test@gmail.com");
    await password.setValue("123456");

    await wrapper.find("form").trigger("submit.prevent");

    expect(store.dispatch).toHaveBeenCalledWith("auth/signInUser", {
      email: "test@gmail.com",
      password: "123456",
    });

    expect(wrapper.router.push).toHaveBeenCalledWith({ name: "no-entry" });
  });
});
