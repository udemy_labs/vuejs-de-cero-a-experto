import createVuexStore from "../../../mock-data/mock-store";
import axios from "axios";

describe("Vuex - Pruebas del Modulo Auth", () => {
  test("debe tener este state inicial", () => {
    const store = createVuexStore({
      status: "not-authenticated", // 'authenticated', 'not-authenticated', 'authenticating'
      user: null,
      idToken: null,
      refreshToken: null,
    });

    const { status, user, idToken, refreshToken } = store.state.auth;

    expect(status).toBe("not-authenticated");
    expect(user).toBe(null);
    expect(idToken).toBe(null);
    expect(refreshToken).toBe(null);
  });

  //testear las mutationes
  test("mutation: loginuser", () => {
    const store = createVuexStore({
      status: "not-authenticated",
      user: null,
      idToken: null,
      refreshToken: null,
    });

    const payload = {
      user: { name: "Test User", email: "test_user@gmail.com" },
      idToken: "ABC-123",
      refreshToken: "XYZ-789",
    };

    store.commit("auth/loginUser", payload);

    const { status, user, idToken, refreshToken } = store.state.auth;
    expect(status).toBe("authenticated");
    expect(user).toEqual({ name: "Test User", email: "test_user@gmail.com" });
    expect(idToken).toBe("ABC-123");
    expect(refreshToken).toBe("XYZ-789");
  });

  test("mutation: logoutUser", () => {
    localStorage.setItem("idToken", "ABC-123");
    localStorage.setItem("refreshToken", "XYZ-789");

    const store = createVuexStore({
      status: "authenticated",
      user: { name: "Test User", email: "test_user@gmail.com" },
      idToken: "ABC-123",
      refreshToken: "XYZ-789",
    });

    store.commit("auth/logoutUser");
    const { status, user, idToken, refreshToken } = store.state.auth;
    expect(status).toBe("not-authenticated");
    expect(user).toBe(null);
    expect(idToken).toBe(null);
    expect(refreshToken).toBe(null);
    expect(localStorage.getItem("idToken")).toBe(null);
    expect(localStorage.getItem("refreshToken")).toBe(null);
  });

  //testear los getters
  test("getters: currentAuthState, username", () => {
    const store = createVuexStore({
      status: "authenticated",
      user: { name: "Test User", email: "test_user@gmail.com" },
      idToken: "ABC-123",
      refreshToken: "XYZ-789",
    });

    expect(store.getters["auth/currentAuthState"]).toBe("authenticated");
    expect(store.getters["auth/username"]).toBe("Test User");
  });

  //testear las actions
  test("Actions: createUser, error el usuario ya existe", async () => {
    const store = createVuexStore({
      status: "not-authenticated", // 'authenticated', 'not-authenticated', 'authenticating'
      user: null,
      idToken: null,
      refreshToken: null,
    });

    const newUser = {
      name: "Test User",
      email: "test_user@gmail.com",
      password: "123456",
    };

    const resp = await store.dispatch("auth/createUser", newUser);
    expect(resp).toEqual({ ok: false, message: "EMAIL_EXISTS" });

    //debido a que el usuario ya existe, no debe haber cambios en el estado
    const { status, user, idToken, refreshToken } = store.state.auth;
    expect(status).toBe("not-authenticated");
    expect(user).toBe(null);
    expect(idToken).toBe(null);
    expect(refreshToken).toBe(null);
    expect(localStorage.getItem("idToken")).toBe(null);
    expect(localStorage.getItem("refreshToken")).toBe(null);
  });

  test("Actions: createUser signInUser, crear y autenticar el usuario", async () => {
    //Esta prueba depende de que el usuario test_user2 exista, para hacer login,
    //eliminarlo y volverlo a crear

    const store = createVuexStore({
      status: "not-authenticated", // 'authenticated', 'not-authenticated', 'authenticating'
      user: null,
      idToken: null,
      refreshToken: null,
    });

    //hacer login
    const userforLogin = {
      email: "test_user2@test.com",
      password: "123456",
    };

    await store.dispatch("auth/signInUser", userforLogin);
    const { idToken } = store.state.auth;

    expect(idToken).toBeTruthy();

    //borrar el usuario
    await axios.post(
      `https://identitytoolkit.googleapis.com/v1/accounts:delete?key=${process.env.VUE_APP_FIREBASE_AUTH_API_KEY}`,
      {
        idToken,
      }
    );

    //crear el usuario
    const newUser = {
      name: "Test User",
      email: "test_user2@test.com",
      password: "123456",
    };

    const createResp = await store.dispatch("auth/createUser", newUser);
    expect(createResp.ok).toBe(true);

    const { status, user, idToken: token, refreshToken } = store.state.auth;
    expect(status).toBe("authenticated");
    expect(user).toMatchObject({
      name: "Test User",
      email: "test_user2@test.com",
    });
    expect(typeof token).toBe("string");
    expect(typeof refreshToken).toBe("string");
  });

  test("Actions: checkAuthState, usuario autenticado", async () => {
    const store = createVuexStore({
      status: "not-authenticated", // 'authenticated', 'not-authenticated', 'authenticating'
      user: null,
      idToken: null,
      refreshToken: null,
    });

    await store.dispatch("auth/signInUser", {
      email: "test_user@gmail.com",
      password: "123456",
    });
    const { idToken } = store.state.auth;
    store.commit("auth/logoutUser");

    localStorage.setItem("idToken", idToken);

    const checkAuthState = await store.dispatch("auth/checkAuthState");
    const { status, user, idToken: token } = store.state.auth;

    expect(checkAuthState).toEqual({ ok: true });
    expect(status).toBe("authenticated");
    expect(user).toMatchObject({
      name: "test user",
      email: "test_user@gmail.com",
    });
    expect(typeof token).toBe("string");
  });

  test("Actions: checkAuthState, usuario No autenticado", async () => {
    const store = createVuexStore({
      status: "not-authenticated", // 'authenticated', 'not-authenticated', 'authenticating'
      user: null,
      idToken: null,
      refreshToken: null,
    });

    localStorage.removeItem("idToken");
    localStorage.removeItem("refreshToken");

    const checkAuthState = await store.dispatch("auth/checkAuthState");
    expect(checkAuthState).toEqual({
      ok: false,
      message: "unauthenticated user",
    });

    localStorage.setItem("idToken", "ABC-123");
    localStorage.getItem("refreshToken", "XYZ-789");
    const checkAuthState2 = await store.dispatch("auth/checkAuthState");
    expect(checkAuthState2).toEqual({
      ok: false,
      message: "INVALID_ID_TOKEN",
    });
  });
});
