import { createStore } from "vuex";
import { shallowMount } from "@vue/test-utils";

import journal from "@/modules/daybook/store/journal";
import journalState from "../../../mock-data/journal-mock-state.js";

import EntryViewVue from "@/modules/daybook/views/EntryView.vue";

import Swal from "sweetalert2";

jest.mock("sweetalert2", () => ({
  fire: jest.fn(),
  showLoading: jest.fn(),
  close: jest.fn(),
}));

const createVuexStore = (initialState) =>
  createStore({
    modules: {
      journal: {
        ...journal,
        state: { ...initialState },
      },
    },
  });

const mockRouter = {
  push: jest.fn(),
};

describe("Pruebas en el EntryView", () => {
  const store = createVuexStore(journalState);
  store.dispatch = jest.fn();

  let wrapper;

  beforeEach(() => {
    jest.clearAllMocks();
    wrapper = shallowMount(EntryViewVue, {
      props: {
        id: "-NpXF9ftUZq84LCdA5Ol",
      },
      global: {
        mocks: {
          $router: mockRouter,
        },
        plugins: [store],
      },
    });
  });

  test("debe sacar al usuario porque el id no existe", () => {
    wrapper = shallowMount(EntryViewVue, {
      props: {
        id: "Este-id-no-existe",
      },
      global: {
        mocks: {
          $router: mockRouter,
        },
        plugins: [store],
      },
    });

    expect(mockRouter.push).toHaveBeenCalledWith({ name: "no-entry" });
  });

  test("debe mostrar la entrada correctamente", () => {
    expect(wrapper.html()).toMatchSnapshot();
    expect(mockRouter.push).not.toHaveBeenCalled();
  });

  test("debe borrar la entrada y salir", (done) => {
    Swal.fire.mockReturnValueOnce(Promise.resolve({ isConfirmed: true }));

    wrapper.find(".btn-danger").trigger("click");

    expect(Swal.fire).toHaveBeenCalledWith({
      title: "¿Esta Seguro?",
      text: "Una vez borrado, no se puede revertir!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, borrar!",
      cancelButtonText: "Cancelar",
    });

    setTimeout(() => {
      expect(store.dispatch).toHaveBeenCalledWith(
        "journal/deleteEntry",
        "-NpXF9ftUZq84LCdA5Ol"
      );
      expect(mockRouter.push).toHaveBeenCalled();
      done();
    }, 1);
  });
});
