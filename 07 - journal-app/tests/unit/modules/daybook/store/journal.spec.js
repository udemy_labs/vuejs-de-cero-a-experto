import { createStore } from "vuex";
import journal from "@/modules/daybook/store/journal";
import journalState from "../../../mock-data/journal-mock-state.js";
import journalAuth from "../../../../../src/api/journalAuth.js";

const createVuexStore = (initialState) =>
  createStore({
    modules: {
      journal: {
        ...journal,
        state: { ...initialState },
      },
    },
  });

describe("Vuex - Pruebas del Modulo Journal", () => {
  beforeAll(async () => {
    const { data } = await journalAuth.post(":signInWithPassword", {
      email: "test_user@gmail.com",
      password: "123456",
      returnSecureToken: true,
    });
    localStorage.setItem("idToken", data.idToken);
  });

  test("este es el estado inicial, debe tener este state", () => {
    const store = createVuexStore(journalState);

    const { isLoading, entries } = store.state.journal;

    expect(isLoading).toBeFalsy();
    expect(entries).toEqual(journalState.entries);
  });

  //Mutations
  test("mutation: setEntries", () => {
    const store = createVuexStore({ isLoading: true, entries: [] });
    store.commit("journal/setEntries", journalState.entries);

    //console.log(store.state.journal.entries);

    expect(store.state.journal.entries.length).toBe(4);
    expect(store.state.journal.isLoading).toBeFalsy();
  });

  test("mutation: updateEntry", () => {
    const store = createVuexStore(journalState);
    const updatedEntry = {
      id: "ABC234",
      date: 162707227978,
      text: "Segunda entrada modificada desde el test",
    };

    store.commit("journal/updateEntry", updatedEntry);

    //console.log(store.state.journal.entries);

    expect(store.state.journal.entries.length).toBe(4);

    const idx = store.state.journal.entries.map((e) => e.id).indexOf("ABC234");
    //console.log(store.state.journal.entries[idx]);

    expect(store.state.journal.entries[idx]).toEqual(updatedEntry);
  });

  test("mutations: addEntry, deleteEntry", () => {
    const store = createVuexStore(journalState);
    const newEntry = {
      id: "ABC456",
      date: 162707227978,
      text: "Cuarta entrada desde el test",
    };

    store.commit("journal/addEntry", newEntry);
    let stateEntries = store.state.journal.entries;
    expect(stateEntries.length).toBe(5);
    expect(stateEntries.find((e) => e.id === newEntry.id)).toBeTruthy();

    store.commit("journal/removeEntry", newEntry.id);
    expect(store.state.journal.entries.length).toBe(4);
    expect(
      store.state.journal.entries.find((e) => e.id === newEntry.id)
    ).toBeUndefined();
  });

  //Getters
  test("getters: getEntriesByTerm, getEntryById", () => {
    const store = createVuexStore(journalState);
    console.log(store.state.journal.entries);

    //console.log(store.getters["journal/getEntriesByTerm"]("mock"));
    expect(store.getters["journal/getEntriesByTerm"]("entrada").length).toBe(3);

    //id: "ABC234",
    //console.log(store.getters["journal/getEntryById"]("ABC234"));
    expect(store.getters["journal/getEntryById"]("ABC234")).toEqual(
      journalState.entries[1]
    );
  });

  ///Actions
  test("actions: loadEntries", async () => {
    const store = createVuexStore({ isLoading: true, entries: [] });

    await store.dispatch("journal/loadEntries");
    console.log(store.state.journal.entries);
    expect(store.state.journal.entries.length).toBeGreaterThan(0);
  });

  test("actions: updateEntry", async () => {
    const store = createVuexStore(journalState);

    const updatedEntry = {
      id: "-NpXF9ftUZq84LCdA5Ol",
      date: 1706750418358,
      picture:
        "https://res.cloudinary.com/dv6bhr9ap/image/upload/v1706925846/images/bxlfgquusfstrrh6motl.jpg",
      text: "No Borrar, Es Necesario para las Pruebas, modificado",
    };

    await store.dispatch("journal/updateEntry", updatedEntry);

    console.log(store.state.journal.entries);
    expect(
      store.state.journal.entries.find((e) => e.id === updatedEntry.id)
    ).toEqual({
      id: "-NpXF9ftUZq84LCdA5Ol",
      date: 1706750418358,
      picture:
        "https://res.cloudinary.com/dv6bhr9ap/image/upload/v1706925846/images/bxlfgquusfstrrh6motl.jpg",
      text: "No Borrar, Es Necesario para las Pruebas, modificado",
    });
  });

  test("actions: create and delete entry", async () => {
    const store = createVuexStore(journalState);

    const newEntry = {
      date: 1706750418358,
      picture:
        "https://res.cloudinary.com/dv6bhr9ap/image/upload/v1706925846/images/undefinedimage.jpg",
      text: "Creado desde los Test",
    };

    const newEntry_id = await store.dispatch("journal/createEntry", newEntry);

    expect(typeof newEntry_id === "string").toBeTruthy();
    expect(
      store.state.journal.entries.find((e) => e.id === newEntry_id)
    ).toBeDefined();

    await store.dispatch("journal/deleteEntry", newEntry_id);

    expect(
      store.state.journal.entries.find((e) => e.id === newEntry_id)
    ).toBeUndefined();
  });
});
