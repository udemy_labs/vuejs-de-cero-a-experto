import journal_router from "@/modules/daybook/router";

describe("Pruebas Router Module del Daybook", () => {
  test("should have this configuration", () => {
    expect(journal_router).toMatchObject({
      name: "daybook",
      component: expect.any(Function),
      children: [
        {
          path: "",
          name: "no-entry",
          component: expect.any(Function),
        },
        {
          path: ":id",
          name: "entry",
          component: expect.any(Function),
          props: expect.any(Function),
        },
      ],
    });
  });

  test("Las rutas deben retornar los componentes de EntryView y NoEntrySelected", async () => {
    //console.log(journal_router);
    //console.log(journal_router.children);
    //console.log(await journal_router.children[0].component());
    //Esta forma es mas estricta por que las rutas deben estar siempre en el mismo orden
    // expect((await journal_router.children[0].component()).default.name).toBe(
    //   "NoEntrySelected"
    // );
    // expect((await journal_router.children[1].component()).default.name).toBe(
    //   "EntryView"
    // );

    //Esta Forma es mas dinamica, ya que no importaria el orden en que esten definidas las rutas
    const promiseRoutes = [];
    journal_router.children.forEach((child) =>
      promiseRoutes.push(child.component())
    );
    const routes = (await Promise.all(promiseRoutes)).map(
      (r) => r.default.name
    );
    expect(routes).toContain("EntryView");
    expect(routes).toContain("NoEntrySelected");
  });

  test("should return id in route", async () => {
    const routeParams = {
      params: {
        id: "ABC-123",
      },
    };

    //console.log(journal_router.children[1].props(route));

    //Forma rigida
    //expect(journal_router.children[1].props(routeParams)).toEqual({ id: "ABC-123" });

    //forma dinamica
    const entryRoute = journal_router.children.find(
      (route) => route.name === "entry"
    );
    console.log(entryRoute);
    expect(entryRoute.props(routeParams)).toEqual({ id: "ABC-123" });
  });
});
