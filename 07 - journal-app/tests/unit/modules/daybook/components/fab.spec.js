import FabVue from "@/modules/daybook/components/Fab.vue";
import { shallowMount } from "@vue/test-utils";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

describe("Fab Component Test", () => {
  test("debe mostrar el icono por defecto", () => {
    const wrapper = shallowMount(FabVue, {
      global: {
        stubs: [FontAwesomeIcon],
      },
    });

    const fai = wrapper.find("font-awesome-icon");

    console.log(fai.attributes());

    expect(fai.attributes("icon")).toBe("fa-plus");
  });

  test("debe mostrar el icono por argumento: fa-circle", () => {
    const wrapper = shallowMount(FabVue, {
      global: {
        stubs: [FontAwesomeIcon],
      },
      props: {
        icon: "fa-circle",
      },
    });

    const fai = wrapper.find("font-awesome-icon");
    expect(fai.attributes("icon")).toBe("fa-circle");
  });

  test("debe emitir el evento on:click cuando se hace click", () => {
    const wrapper = shallowMount(FabVue, {
      global: {
        stubs: [FontAwesomeIcon],
      },
    });
    wrapper.find("button").trigger("click");

    expect(wrapper.emitted("on:click")).toHaveBeenCalled;
  });
});
