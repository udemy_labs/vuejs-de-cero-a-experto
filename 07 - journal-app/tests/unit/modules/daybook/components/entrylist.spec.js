import { createStore } from "vuex";
import { shallowMount } from "@vue/test-utils";

import journal from "@/modules/daybook/store/journal";
import journalState from "../../../mock-data/journal-mock-state.js";

import EntryListVue from "@/modules/daybook/components/EntryList.vue";

const createVuexStore = (initialState) =>
  createStore({
    modules: {
      journal: {
        ...journal,
        state: { ...initialState },
      },
    },
  });

const mockRouter = {
  push: jest.fn(),
};

describe("pruebas en el componente entrylist", () => {
  /*
  //Primera forma de hacerlo mas trabajosa, pero tambien mas detallada
  const journalMockModule = {
    namespaced: true,
    getters: {
      //getEntriesByTerm: jest.fn(),
      getEntriesByTerm,
    },
    state: { ...journalState },
  };

  const store = createStore({
    modules: {
      journal: { ...journalMockModule },
    },
  });
*/

  const store = createVuexStore(journalState);

  let wrapper;

  beforeEach(() => {
    jest.clearAllMocks();
    wrapper = shallowMount(EntryListVue, {
      global: {
        mocks: {
          $router: mockRouter,
        },
        plugins: [store],
      },
    });
  });

  test("debe llamar el getEntriesByTerm sin terminos y mostrar 4 entradas", () => {
    //console.log(wrapper.html());
    expect(wrapper.html).toMatchSnapshot();
    expect(wrapper.findAll("entry-stub").length).toBe(4);
  });

  test("debe llamar el getEntriesByTerm y filtrar las entradas", async () => {
    const input = wrapper.find("input");
    await input.setValue("Segunda");

    //console.log(wrapper.html());
    expect(wrapper.findAll("entry-stub").length).toBe(1);
  });

  test("el boton de nuevo debe redireccionar a /new", () => {
    wrapper.find("button").trigger("click");
    expect(mockRouter.push).toHaveBeenCalled();
    expect(mockRouter.push).toHaveBeenCalledWith({
      name: "entry",
      params: { id: "new" },
    });
  });
});
