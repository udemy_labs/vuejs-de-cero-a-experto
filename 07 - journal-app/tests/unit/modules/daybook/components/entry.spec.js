import { shallowMount } from "@vue/test-utils";
import Entry from "@/modules/daybook/components/Entry.vue";
import journalState from "../../../mock-data/journal-mock-state.js";

describe("Pruebas del Componente Entry", () => {
  const mockRouter = {
    push: jest.fn(),
  };

  const wrapper = shallowMount(Entry, {
    props: {
      entry: journalState.entries[0],
    },
    global: {
      mocks: {
        $router: mockRouter,
      },
    },
  });

  test("debe hacer match con el snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  test("debe redireccionar al hacer click en el div entry-container", () => {
    const entryContainer = wrapper.find(".entry-container");
    entryContainer.trigger("click");
    expect(mockRouter.push).toHaveBeenCalled();
    expect(mockRouter.push).toHaveBeenCalledWith({
      name: "entry",
      params: {
        id: journalState.entries[0].id,
      },
    });
  });

  test("debe en la propiedades computadas", () => {
    //wrapper.vm. --> aqui se pueden ver las propiedades computadas
    //day: 25
    //month: Julio
    //yearDate: '2021, Viernes'

    console.log(wrapper.vm.dayText);
    expect(wrapper.vm.dayText).toBe(27);

    console.log(wrapper.vm.monthText);
    expect(wrapper.vm.monthText).toBe("Febrero");

    console.log(wrapper.vm.yearDayText);
    expect(wrapper.vm.yearDayText).toBe("1975, Jueves");
  });
});
