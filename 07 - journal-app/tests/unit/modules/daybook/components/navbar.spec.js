import Navbar from "@/modules/daybook/components/Navbar.vue";
import { shallowMount } from "@vue/test-utils";
import createVuexStore from "../../../mock-data/mock-store";

//Esto es para que las pruebas del router no fallen con el composition api
import {
  VueRouterMock,
  createRouterMock,
  injectRouterMock,
} from "vue-router-mock";
import { config } from "@vue/test-utils";

// create one router per test file
const router = createRouterMock();
beforeEach(() => {
  router.reset(); // reset the router state
  injectRouterMock(router);
});

// Add properties to the wrapper
config.plugins.VueWrapper.install(VueRouterMock);
//Fin de la configuracion para que las pruebas del router no fallen con el compisotion api

describe("Daybook Navbar Component", () => {
  const store = createVuexStore({
    user: {
      name: "Test User",
      email: "test@test.com",
    },
    status: "authenticated",
    idToken: "ABC-123",
    refreshToken: "XYZ-789",
  });
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test("debe hacer match con el snapshot", () => {
    const wrapper = shallowMount(Navbar, {
      global: {
        plugins: [store],
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  test("debe llamar al logout", async () => {
    const wrapper = shallowMount(Navbar, {
      global: {
        plugins: [store],
      },
    });

    await wrapper.find("button").trigger("click");

    expect(store.state.auth).toEqual({
      user: null,
      status: "not-authenticated",
      idToken: null,
      refreshToken: null,
    });

    expect(wrapper.router.push).toHaveBeenCalledWith({ name: "login" });
    //expect(store.dispatch).toHaveBeenCalledWith("auth/logoutUser");
  });
});
