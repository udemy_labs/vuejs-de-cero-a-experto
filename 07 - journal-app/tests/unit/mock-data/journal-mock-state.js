const journalState = {
  isLoading: false,
  entries: [
    {
      id: "ABC123",
      date: 162707227978,
      text: "Primera entrada desde mock data",
    },
    {
      id: "ABC234",
      date: 162707227978,
      text: "Segunda entrada desde mock data",
    },
    {
      id: "ABC345",
      date: 162707227978,
      text: "Tercera entrada desde mock data",
    },
    {
      id: "-NpXF9ftUZq84LCdA5Ol",
      date: 1706750418358,
      picture:
        "https://res.cloudinary.com/dv6bhr9ap/image/upload/v1706925846/images/bxlfgquusfstrrh6motl.jpg",
      text: "Este Viene desde el Firebase",
    },
  ],
};

export default journalState;
