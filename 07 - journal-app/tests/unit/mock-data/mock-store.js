import { createStore } from "vuex";
import journal from "@/modules/daybook/store/journal";
import auth from "@/modules/auth/store";
import journalState from "./journal-mock-state.js";

const createVuexStore = (
  authinitialState,
  journalinitialState = journalState
) => {
  return createStore({
    modules: {
      journal: {
        ...journal,
        state: { ...journalinitialState },
      },
      auth: {
        ...auth,
        state: { ...authinitialState },
      },
    },
  });
};
export default createVuexStore;
