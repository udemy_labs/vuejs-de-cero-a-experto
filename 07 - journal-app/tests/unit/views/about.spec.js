import AboutView from "@/views/AboutView.vue";
import { shallowMount } from "@vue/test-utils";

describe("About View Testing", () => {
  test("debe hacer match con el snapshot", () => {
    const wrapper = shallowMount(AboutView);
    expect(wrapper.html()).toMatchSnapshot();
  });
});
