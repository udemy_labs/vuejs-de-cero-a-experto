import { computed } from "vue";
import { useStore } from "vuex";

const useAuth = () => {
  const store = useStore();

  const createUser = async (user) => {
    const resp = await store.dispatch("auth/createUser", user);
    return resp;
  };

  const signInUser = async (user) => {
    const resp = await store.dispatch("auth/signInUser", user);
    return resp;
  };

  const checkAuthState = async () => {
    const resp = await store.dispatch("auth/checkAuthState");
    return resp;
  };

  const logoutUser = async () => {
    await store.dispatch("auth/logoutUser");
    await store.dispatch("journal/clearEntries");
  };

  return {
    createUser,
    signInUser,
    checkAuthState,
    authStatus: computed(() => store.getters["auth/currentAuthState"]),
    logoutUser,
    username: computed(() => store.getters["auth/username"]),
  };
};

export default useAuth;
