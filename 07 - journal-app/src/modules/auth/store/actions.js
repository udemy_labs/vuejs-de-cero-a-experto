// export const myActions = async ({ commit }) => {};

import journalAuth from "@/api/journalAuth";

export const createUser = async ({ commit }, user) => {
  const { name, email, password } = user;
  try {
    const { data } = await journalAuth.post(":signUp", {
      email,
      password,
      returnSecureToken: true,
    });

    const { idToken, refreshToken } = data;

    await journalAuth.post(":update", {
      idToken,
      displayName: name,
    });

    delete user.password;
    commit("loginUser", { user, idToken, refreshToken });

    return { ok: true };
  } catch (error) {
    return { ok: false, message: error.response.data.error.message };
  }
};

export const signInUser = async ({ commit }, user) => {
  const { email, password } = user;
  try {
    const { data } = await journalAuth.post(":signInWithPassword", {
      email,
      password,
      returnSecureToken: true,
    });

    const { displayName, idToken, refreshToken } = data;
    user.name = displayName;
    delete user.password;
    commit("loginUser", { user, idToken, refreshToken });
    return { ok: true };
  } catch (error) {
    return { ok: false, message: error.response.data.error.message };
  }
};

export const checkAuthState = async ({ commit }) => {
  const idToken = localStorage.getItem("idToken");
  const refreshToken = localStorage.getItem("refreshToken");

  if (!idToken) {
    commit("logoutUser");
    return { ok: false, message: "unauthenticated user" };
  }

  try {
    const { data } = await journalAuth.post(":lookup", {
      idToken,
    });

    const { displayName, email } = data.users[0];

    const user = {
      name: displayName,
      email,
    };

    commit("loginUser", { user, idToken, refreshToken });
    return { ok: true };
  } catch (error) {
    commit("logoutUser");
    return { ok: false, message: error.response.data.error.message };
  }
};

export const logoutUser = async ({ commit }) => {
  commit("logoutUser");
};
