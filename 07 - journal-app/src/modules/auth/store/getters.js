// export const myGetter = (state) => {
//   return state;
// };

export const currentAuthState = (state) => {
  return state.status;
};
export const username = (state) => {
  return state.user ? state.user.name : null;
};
