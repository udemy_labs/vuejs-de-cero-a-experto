// export const myActions = async ({ commit }) => {};

import journalApi from "@/api/journalApi";

export const loadEntries = async ({ commit }) => {
  const { data } = await journalApi.get("/entries.json");

  if (!data) {
    commit("setEntries", []);
    return;
  }

  const entries = [];
  for (let id of Object.keys(data)) {
    entries.push({
      id,
      ...data[id],
    });
  }

  commit("setEntries", entries);
};

export const updateEntry = async ({ commit }, entry) => {
  const urlPath = "/entries/" + entry.id + ".json";
  const { date, text, picture } = entry;
  const payload = { date, text, picture };
  await journalApi.put(urlPath, payload);

  payload.id = entry.id;

  commit("updateEntry", { ...payload });
};

export const createEntry = async ({ commit }, entry) => {
  const urlPath = "/entries.json";
  const { date, text, picture } = entry;
  const payload = { date, text, picture };
  const { data } = await journalApi.post(urlPath, payload);

  payload.id = data.name;

  commit("addEntry", { ...payload });

  return data.name;
};

export const deleteEntry = async ({ commit }, entryId) => {
  const urlPath = "/entries/" + entryId + ".json";
  await journalApi.delete(urlPath);
  commit("removeEntry", entryId);
};

export const clearEntries = async ({ commit }) => {
  commit("clearEntries");
};
