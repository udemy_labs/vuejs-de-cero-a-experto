//export const myMutation = (state) => {};

export const setEntries = (state, entries) => {
  state.entries = [...state.entries, ...entries];
  state.isLoading = false;
};

export const updateEntry = (state, entry) => {
  const idx = state.entries.map((e) => e.id).indexOf(entry.id);
  state.entries[idx] = entry;
};

export const addEntry = (state, entry) => {
  state.entries.unshift(entry);
  state.isLoading = false;
};

export const removeEntry = (state, entryId) => {
  const idx = state.entries.map((e) => e.id).indexOf(entryId);
  state.entries.splice(idx, 1);
};
export const clearEntries = (state) => {
  state.entries = [];
  state.isLoading = false;
};
