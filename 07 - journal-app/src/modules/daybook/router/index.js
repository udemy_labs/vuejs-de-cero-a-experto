export default {
  name: "daybook",
  component: () =>
    import(
      /* webpackChunkName: "daybook" */ "@/modules/daybook/layouts/DayBookLayout.vue"
    ),
  children: [
    {
      path: "",
      name: "no-entry",
      component: () =>
        import(
          /* webpackChunkName: "no-entry" */ "@/modules/daybook/views/NoEntrySelected.vue"
        ),
    },
    {
      path: ":id",
      name: "entry",
      component: () =>
        import(
          /* webpackChunkName: "entry" */ "@/modules/daybook/views/EntryView.vue"
        ),
      props: (route) => {
        const { id } = route.params;
        return id.trim().length === 0
          ? { redirect: { name: "no-entry" } }
          : { id: id };
      },
    },
  ],
};

/*
props: (route) => {
    const { id } = route.params;
    return isNaN(Number(id))
      ? { redirect: { name: "404" } }
      : { id: Number(id) };
  },
*/
