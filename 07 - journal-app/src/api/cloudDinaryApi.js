import axios from "axios";

const cloudDinaryApi = axios.create({
  baseURL: "https://api.cloudinary.com/v1_1/dv6bhr9ap/image/upload",
});

export default cloudDinaryApi;
