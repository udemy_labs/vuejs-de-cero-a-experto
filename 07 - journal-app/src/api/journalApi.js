import axios from "axios";

const journalApi = axios.create({
  baseURL: process.env.VUE_APP_FIREBASE_API_BASE_URL,
});

journalApi.interceptors.request.use((config) => {
  const token = localStorage.getItem("idToken");

  console.log(config.params, token);
  config.params = {
    auth: token,
  };

  return config;
});

export default journalApi;
