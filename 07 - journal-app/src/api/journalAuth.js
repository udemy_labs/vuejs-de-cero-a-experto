import axios from "axios";

const journalAuth = axios.create({
  baseURL: process.env.VUE_APP_FIREBASE_AUTH_BASE_URL,
  params: {
    key: process.env.VUE_APP_FIREBASE_AUTH_API_KEY,
  },
});

export default journalAuth;
