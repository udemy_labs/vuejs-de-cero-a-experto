import { computed } from "vue";
import { useStore } from "vuex";

const useUI = () => {
  const store = useStore();

  const toggleSideMenu = () => {
    store.dispatch("ui/toggleSideMenu");
  };

  return {
    toggleSideMenu,
    // currentSideMenuState: computed(
    //   () => store.getters["ui/currentSideMenuState"]
    // ),
    currentSideMenuState: computed({
      get() {
        return store.getters["ui/currentSideMenuState"];
      },
      set(val) {
        store.dispatch("ui/toggleSideMenu");
      },
    }),
  };
};

export default useUI;
