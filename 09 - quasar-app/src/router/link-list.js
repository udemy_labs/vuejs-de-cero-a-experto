export const linksList = [
  {
    title: "Typography",
    caption: "Tipod de letra en quasar",
    icon: "las la-font",
    link: "typography",
  },
  {
    title: "Flex Styles",
    caption: "Estilos con Flex",
    icon: "las la-layer-group",
    link: "flex",
  },
  {
    title: "Dialogs",
    caption: "Alerts, Dialogs and confirmations",
    icon: "las la-window-minimize",
    link: "dialogs",
  },
  {
    title: "Forms",
    caption: "Forms, inputs and validations",
    icon: "lab la-wpforms",
    link: "forms",
  },
  {
    title: "Docs",
    caption: "quasar.dev",
    icon: "las la-graduation-cap",
    link: "https://quasar.dev",
  },
];
