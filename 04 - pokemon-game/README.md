# pokemon-game

4to Ejercicio del Curso de Cero a Experto por Fernando Herrera
Seccion 7 y 8 : Juego de Adivina el Pokemon y sus Unit Testing

Recursos Externos Utilizados:
https://jestjs.io/docs/api
https://pokeapi.co/api/v2/pokemon

Fuente: Udemy/DevTalles

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Update unit tests snapshots

```
npm run jest-updateSnap
```
