import pokemonApi from "@/api/pokemonApi";

export const getPokemons = () => {
  const pokemonsArr = Array.from(Array(650));
  return pokemonsArr.map((_, index) => index + 1);
};

const getPokemonOptions = async () => {
  const mixedPokemons = getPokemons().sort(() => Math.random() - 0.5);
  const pokemons = await getPokemonNames(mixedPokemons.slice(0, 4));
  return pokemons;
};

export const getPokemonNames = async ([a, b, c, d] = []) => {
  const promisesArr = [
    pokemonApi.get(`/${a}`),
    pokemonApi.get(`/${b}`),
    pokemonApi.get(`/${c}`),
    pokemonApi.get(`/${d}`),
  ];

  const [pokemon1, pokemon2, pokemon3, pokemon4] = await Promise.all(
    promisesArr
  );

  return [
    { name: pokemon1.data.name, id: pokemon1.data.id },
    { name: pokemon2.data.name, id: pokemon2.data.id },
    { name: pokemon3.data.name, id: pokemon3.data.id },
    { name: pokemon4.data.name, id: pokemon4.data.id },
  ];
};

//EN ESTA FORMA HAGO TODO EN UN SOLO METODO, PERO POR EL TEMA DE LAS
//PRUEBAS UNITARIAS, EL CURSO PREFIERE DIVIDIRLO EN 3 PASOS DIFERENTES
// const getPokemonOptions = async () => {
//   const pokemonsArr = Array.from(Array(650)).map((_, index) => index + 1);
//   const mixedPokemons = pokemonsArr.sort(() => Math.random() - 0.5);
//   const [a, b, c, d] = mixedPokemons.slice(0, 4);

//   const promisesArr = [
//     pokemonApi.get(`/${a}`),
//     pokemonApi.get(`/${b}`),
//     pokemonApi.get(`/${c}`),
//     pokemonApi.get(`/${d}`),
//   ];

//   const [p1, p2, p3, p4] = await Promise.all(promisesArr);

//   return [
//     { name: p1.data.name, id: p1.data.id },
//     { name: p2.data.name, id: p2.data.id },
//     { name: p3.data.name, id: p3.data.id },
//     { name: p4.data.name, id: p4.data.id },
//   ];
// };

export default getPokemonOptions;
