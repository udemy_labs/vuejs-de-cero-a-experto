import { shallowMount } from "@vue/test-utils";
import PokemonPicture from "@/components/PokemonPicture.vue";

describe("PokemonPicture", () => {
  test("debe hacer match con el snapshot", () => {
    const wrapper = shallowMount(PokemonPicture, {
      props: { pokemonId: 56, showPokemon: false },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  test("debe mostrar la imagen oculta y el pokemon 100", () => {
    const wrapper = shallowMount(PokemonPicture, {
      props: { pokemonId: 100, showPokemon: false },
    });

    const img = wrapper.findAll("img");
    //como se que solo debe estar renderizada una sola imagen cualquiera de las siguientes
    //es una forma validad de comprobar que solo se haya renderizado una imagen
    expect(img.length).toBe(1);
    expect(img[1]).toBe(undefined);

    //estas son dos formas de buscar que haya un elemento con la clase hidden-pokemon
    //la primera es como basica, y la segunda es mas precisa por que incluso confirmo que es la primera,
    //y en este caso, unica img la que tenga la clase asignada
    expect(wrapper.html()).toContain('class="hidden-pokemon"');
    expect(img[0].classes("hidden-pokemon")).toBeTruthy();

    //Compruebo que el pokemon sea el 100 ya que el url se armo con ese valor que se paso en las props
    //ambas opciones son validas
    expect(wrapper.html()).toContain("100.svg");
    expect(img[0].attributes("src")).toBe(
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/100.svg"
    );
  });

  test("debe mostrar la imagen de pokemon cuando showPokemon es true", () => {
    const wrapper = shallowMount(PokemonPicture, {
      props: { pokemonId: 100, showPokemon: true },
    });

    //busco la primera img, como se que solo se debe renderizar una, esto es valido
    const img = wrapper.find("img");
    expect(img.exists()).toBeTruthy();

    expect(img.classes("hidden-pokemon")).toBe(false);
    expect(img.classes("fade-in")).toBe(true);
  });
});
