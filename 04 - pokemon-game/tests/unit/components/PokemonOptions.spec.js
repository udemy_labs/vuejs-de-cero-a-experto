import { shallowMount } from "@vue/test-utils";
import PokemonOptions from "@/components/PokemonOptions.vue";
import { pokemons } from "../mocks/pokemons.mock";

describe("PokemonOptions Component", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallowMount(PokemonOptions, {
      props: {
        pokemons,
      },
    });
  });

  test("debe hacer match con el snapshot", () => {
    expect(wrapper.html()).toMatchSnapshot();
  });

  test("debe de mostrar las cuatro opciones correctamentes", () => {
    const liTags = wrapper.findAll("li");

    expect(liTags.length).toBe(4);

    expect(liTags[0].text()).toBe("bulbasaur");
    expect(liTags[1].text()).toBe("ivysaur");
    expect(liTags[2].text()).toBe("venusaur");
    expect(liTags[3].text()).toBe("charmander");
  });

  test('debe emitir "selection" con sus respectivos parametros al hacer clic', () => {
    const [li1, li2, li3, li4] = wrapper.findAll("li");
    li1.trigger("click");
    expect(wrapper.emitted("selection").length).toBe(1);
    expect(wrapper.emitted("selection")[0]).toEqual([1]);
  });
});
