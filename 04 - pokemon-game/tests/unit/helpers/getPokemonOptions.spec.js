import { shallowMount } from "@vue/test-utils";
import getPokemonOptions, {
  getPokemons,
  getPokemonNames,
} from "@/helpers/pokemonOptions";

describe("getPokemonOptions helpers", () => {
  test("debe regresar un arreglo de numeros", () => {
    const pokemons = getPokemons();
    expect(pokemons.length).toBe(650);
    expect(pokemons[0]).toBe(1);
    expect(pokemons[649]).toBe(650);
  });

  test("debe regresar un arreglo de 4 elementos", async () => {
    const pokemons = await getPokemonNames([1, 2, 3, 4]);
    expect(pokemons.length).toBe(4);
    expect(
      pokemons[0].name === "bulbasaur" &&
        pokemons[1].name === "ivysaur" &&
        pokemons[2].name === "venusaur" &&
        pokemons[3].name === "charmander"
    ).toBe(true);
  });

  test("getPokemonOptions debe retornar un arreglo mesclado", async () => {
    const pokemons = await getPokemonOptions();
    expect(pokemons.length).toBe(4);
    expect(pokemons).toEqual([
      { name: expect.any(String), id: expect.any(Number) },
      { name: expect.any(String), id: expect.any(Number) },
      { name: expect.any(String), id: expect.any(Number) },
      { name: expect.any(String), id: expect.any(Number) },
    ]);
  });
});
