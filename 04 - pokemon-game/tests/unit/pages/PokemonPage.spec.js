import { mount, shallowMount } from "@vue/test-utils";
import PokemonPage from "@/pages/PokemonPage.vue";
import { pokemons } from "../mocks/pokemons.mock";

describe("Pokemon Page", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(PokemonPage);
  });

  test("Debe llamar al mixPokemonArray", () => {
    const mixPokemonArraySpy = jest.spyOn(
      PokemonPage.methods,
      "mixPokemonArray"
    );
    const wrapper = shallowMount(PokemonPage);

    expect(mixPokemonArraySpy).toHaveBeenCalled();
  });

  test("debe de hacer match con el snapshot cuando cargan los pokemons", () => {
    const wrapper = shallowMount(PokemonPage, {
      data() {
        return {
          pokemonArr: pokemons,
          pokemon: pokemons[0],
          showPokemon: false,
          answered: false,
          message: "",
        };
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  test("debe mostrar los componentes de pokemon picture y pokemon options", () => {
    const wrapper = mount(PokemonPage, {
      data() {
        return {
          pokemonArr: pokemons,
          pokemon: pokemons[0],
          showPokemon: false,
          answered: false,
          message: "",
        };
      },
    });

    expect(wrapper.html()).toMatchSnapshot();

    //PokemonPicture debe existir
    expect(wrapper.find(".pokemon-container").exists()).toBeTruthy();

    //PokemonOptions debe existir
    expect(wrapper.find(".options-container").exists()).toBeTruthy();

    //PokemonPicture attribute pokemon Id === '1'
    expect(wrapper.html()).toContain("1.svg");

    //PokemonOptions attribute pokemons toBe true
    const liTags = wrapper.findAll("li");

    expect(liTags.length).toBe(4);

    expect(liTags[0].text()).toBe("bulbasaur");
    expect(liTags[1].text()).toBe("ivysaur");
    expect(liTags[2].text()).toBe("venusaur");
    expect(liTags[3].text()).toBe("charmander");
  });

  test("pruebas con checkAnswer", async () => {
    const wrapper = shallowMount(PokemonPage, {
      data() {
        return {
          pokemonArr: pokemons,
          pokemon: pokemons[0],
          showPokemon: false,
          answered: false,
          message: "",
        };
      },
    });

    await wrapper.vm.checkAnswer(1);

    expect(wrapper.html()).toMatchSnapshot();

    expect(wrapper.find("h2").exists()).toBeTruthy();

    expect(wrapper.vm.showPokemon).toBeTruthy();

    await wrapper.vm.checkAnswer(10);

    expect(wrapper.find("h2").text()).toBe(
      `D'oh! Fallaste, era ${pokemons[0].name}`
    );
  });
});
