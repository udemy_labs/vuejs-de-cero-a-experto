const quotes = [
  {
    quote:
      "The night is darkest just before the dawn. And I promise you, the dawn is coming.",
    author: "Harvey Dent, The Dark Knight",
  },
  {
    quote: "I believe what doesn’t kill you simply makes you, stranger.",
    author: "The Joker, The Dark Knight",
  },
  {
    quote:
      "Your anger gives you great power. But if you let it, it will destroy you… As it almost did me",
    author: "Henri Ducard, Batman Begins",
  },
  {
    quote:
      "You either die a hero or live long enough to see yourself become the villain.",
    author: "Harvey Dent, The Dark Knight",
  },
  {
    quote: "If you’re good at something, never do it for free.",
    author: "The Joker, The Dark Knight",
  },
  {
    quote: "Yes, father. I shall become a bat.",
    author: "Bruce Wayne/Batman, Batman: Year One",
  },
];

const app = Vue.createApp({
  template: `

   <h1>Frases de Batman</h1>
   <input type="text" v-model="newQuote"  v-on:keypress.enter="addQuote" >
   <button type="button" v-on:click="addQuote">Agregar Frase</button>
   <hr>
   <ul>
      <li v-for="(quote, index) in quotes">
      <span>{{ index+1}} {{ quote.quote }}</span>
      <blockquote v-if="quote.author">- <i>{{ quote.author}} </i></blockquote>
      </li>
   </ul>
  
   <h1>Frases de Batman</h1>
   <h3>Ejemplo desestructurado</h3>
   <ul>
      <li v-for="({quote,author},index) in quotes">
      <span>{{ index+1}} {{ quote }}</span>

      <blockquote v-show="author">- <i>{{ author}} </i></blockquote>
      </li>
   </ul> 
   
    
    `,
  data() {
    return {
      quotes: quotes,
      newQuote: "",
    };
  },
  methods: {
    addQuote() {
      if (this.newQuote) {
        console.log(this.newQuote);

        this.quotes.push({ quote: this.newQuote });
        this.newQuote = "";
      }
    },
  },
});

app.mount("#myApp");
