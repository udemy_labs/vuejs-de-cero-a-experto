const app = Vue.createApp({
  template: `
    <h1>Hola Mundo</h1>
    <h2>{{ 'desde data ' + msg }}</h2>
    <blockquote>{{ '-'+autor}}</blockquote>
    <p>{{  true ? 'ACTIVO' : 'INACTIVO' }}</p>
    <button type="button" v-on:click="changeQuote">Click Me!</button>
    <button type="button" v-on:click="capitalize">Capitalize Me!</button>
    `,
  data() {
    return {
      msg: "Hola Mundo",
      autor: "Pues Yo",
    };
  },
  methods: {
    changeQuote() {
      this.msg = "Hola Mundo Cambiado desde un Boton";
      console.log("Hola Mundo desde un metodo");
    },
    capitalize() {
      this.autor = this.autor.toUpperCase();
    },
  },
});

app.mount("#myApp");
