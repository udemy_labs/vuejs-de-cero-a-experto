import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//fontawesome imports
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
//swetalert2 imports
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
//bootstrap imports
import "./styles/styles.scss";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';


library.add(fas, fab, far);

if (!navigator.geolocation) {
    console.log("Geolocation is not supported by your browser");
    alert("Geolocation is not supported by your browser");
    throw new Error("Geolocation is not supported by your browser");
}

import mapboxgl from 'mapbox-gl'; 
mapboxgl.accessToken = process.env.VUE_APP_MAPBOX_TOKEN;

createApp(App)
    .component("font-awesome-icon", FontAwesomeIcon)
    .use(VueSweetalert2)
    .use(store)
    .use(router)
    .mount('#app')
