import axios from 'axios';

const searchApi = axios.create({
    baseURL: process.env.VUE_APP_MAPBOX_BASE_URL + process.env.VUE_APP_MAPBOX_GEOCODING_URL,
    params: {
        limit: 5,
        language: 'es',
        access_token: process.env.VUE_APP_MAPBOX_TOKEN
    }
});
    
export default searchApi;