import axios from 'axios';

const directionsApi = axios.create({
    baseURL: process.env.VUE_APP_MAPBOX_BASE_URL + process.env.VUE_APP_MAPBOX_DIRECTIONS_URL,
    params: {
        alternatives: false,
        geometries: 'geojson',
        overview: 'simplified',
        steps: false,   
        access_token: process.env.VUE_APP_MAPBOX_TOKEN
    }
});
    
export default directionsApi;