import { defineComponent,ref, computed} from "vue";
import SearchResult from "../searchresults/SearchResult.vue";
import { useMapStore, usePlacesStore } from "@/composables";

export default defineComponent({
    name: 'SearchBar',
    components: {
        SearchResult
    },
    setup() {
         const { isUserLocationReady, searchPlacesByTerm } = usePlacesStore();
        const { isMapReady } = useMapStore();
        const debounceTimeout = ref();
        const searchQuery = ref('');
        
        return {
            searchQuery,
            isSearchBarVisible: computed(
                () => isUserLocationReady.value && isMapReady.value
            ),
            debouncedSearchQuery: computed({
                get: () => searchQuery.value,
                set: (value: string) => {
                    if (debounceTimeout.value) 
                        clearTimeout(debounceTimeout.value);
                    
                    debounceTimeout.value = setTimeout(() => {
                        searchQuery.value = value;
                        searchPlacesByTerm(value);
                    }, 500);
                }
            })
        }
    }
});