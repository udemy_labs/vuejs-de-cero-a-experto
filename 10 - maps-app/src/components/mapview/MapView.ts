import { defineComponent,onMounted,ref, watch } from 'vue';
import { usePlacesStore } from '@/composables/usePlacesStore';
import { useMapStore } from '@/composables/useMapStore';
import mapboxgl from 'mapbox-gl';


export default defineComponent({
    name: 'MapView',
    setup() {
        const { userLocation, isUserLocationReady, } = usePlacesStore();
        const { setMap } = useMapStore();
        const mapContainer = ref<HTMLDivElement>();
        

        const initMap = () => {
            if (!mapContainer.value) {
                console.error('Map container is not ready');
                return;
            }
            if (!userLocation.value) {
                console.error('User location is not ready');
                return;
            }   
            const map = new mapboxgl.Map({
                container: mapContainer.value, // container ID
                style: 'mapbox://styles/mapbox/satellite-streets-v12', // style URL
                center: userLocation.value, // starting position [lng, lat]
                zoom: 15, // starting zoom
            });

            const myLocationPopup = new mapboxgl.Popup({ offset: 25 })
                .setLngLat(userLocation.value)
                .setHTML('<span>Usted esta aqui!</span>')

            const myLocationMarker = new mapboxgl.Marker()
                .setLngLat(userLocation.value)
                .setPopup(myLocationPopup)
                .addTo(map);
            setMap(map);
        }
        
        onMounted(() => {
            if(isUserLocationReady.value){
                initMap();
            }
        });

        watch(isUserLocationReady, (newValue) => {
            if(newValue){
                initMap();
            }
        });

        return {
            isUserLocationReady,
            userLocation,
            mapContainer
        }
    }
});