import { defineComponent,ref, watch } from 'vue';
import { usePlacesStore } from '@/composables';
import { Feature } from '@/interaces/places';
import { useMapStore } from '@/composables';

export default defineComponent({
name: 'SearchResult',
    setup() {
        const { isLoadingPlaces, places, userLocation } = usePlacesStore();
        const { map, setPlaceMarkers, getRouteBetweenPoints } = useMapStore();
        const activePlace = ref('');

        watch(places, (newValue) => { 
            activePlace.value = '';
            setPlaceMarkers(newValue);
        });

return {
    isLoadingPlaces,
    places,
    activePlace,
    onPlaceClick: (place: Feature) => { 
       activePlace.value = place.id;  
        const [lng, lat] = place.center;

        map.value?.flyTo({
            center: [lng, lat],
            zoom:14,    
        });
    },
    onPlaceDetailsClick: (place: Feature) => {
        if (!userLocation.value) {
            return;
        }

        const [lng, lat] = place.center;
        const [userLng, userLat] = userLocation.value;

        const origin: [number, number]  = [userLng, userLat];
        const destination: [number, number]  = [lng, lat];
        
        getRouteBetweenPoints({origin, destination});
    }

};
},
});