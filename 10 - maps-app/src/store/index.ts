
import { createStore } from 'vuex';

// My custom modules
// import exampleModule from './module-template';
// import { ExampleStateInterface } from './module-template/state';

import placesModule  from './places';
import { PlacesState } from './places/state';

import mapModule from './maps';
import { MapState } from './maps/state';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface StateInterface {
  places: PlacesState;
  map: MapState;
}

export default createStore<StateInterface>({
  modules: {
    places: placesModule,
    map: mapModule,
  }
})