import { MutationTree } from "vuex";
import { PlacesState } from "./state";
import { Feature } from '../../interaces/places';

const mutation: MutationTree<PlacesState> = {
  setInitialLocation(state: PlacesState, payload: { lng: number; lat: number }) {
    state.userLocation = [payload.lng, payload.lat];
    state.isLoading = false;
  },

  setIsLoadingPlaces(state) {
    state.isLoadingPlaces = true;
  },

  setPlaces(state, payload: Feature[]) {
    state.places = payload;
    state.isLoadingPlaces = false;
  }
};

export default mutation;
