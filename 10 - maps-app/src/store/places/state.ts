import { Feature } from "@/interaces/places";

export interface PlacesState {
    isLoading: boolean;
    userLocation?: [number, number];//latitude,longitude
    isLoadingPlaces: boolean;
    places: Feature[];
}

function state(): PlacesState {
    return {
        isLoading: true,
        userLocation: undefined,
        isLoadingPlaces: false,
        places: [],
    }
}

export default state;