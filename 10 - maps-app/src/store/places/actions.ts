import { ActionTree } from "vuex";
import { PlacesState } from "./state";
import { StateInterface } from "../index";
import { searchApi } from "@/apis";
import { Feature,PlacesResponse } from "@/interaces/places";

const actions: ActionTree<PlacesState, StateInterface> = {
  getInitialLocation({ commit }) {
    //todo: colocar loading
    navigator.geolocation.getCurrentPosition(
      (position) => {
        commit("setInitialLocation", {
          lng: position.coords.longitude,
          lat: position.coords.latitude,
        });
      },
      (error) => {
        console.error(error);
        throw new Error("Não foi possível obter a localização inicial");
      }
    );
  },

  async searchPlacesByTerm({ commit, state }, term: string): Promise<Feature[]> {

    if (term.length === 0) {
      commit("setPlaces", []);
      return [];
    }

    if (!state.userLocation) {
      throw new Error("Localização do usuário não disponível");
    }

    commit("setIsLoadingPlaces");
    const response = await searchApi.get<PlacesResponse>(`/${term}.json`, {
      params: {
        proximity: state.userLocation?.join(","),
      }
    });

    commit("setPlaces", response.data.features);
    return response.data.features;
  },
};

export default actions;
