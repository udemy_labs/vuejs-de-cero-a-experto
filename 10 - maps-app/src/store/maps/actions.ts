import { ActionTree } from "vuex";
import { MapState } from "./state";
import { StateInterface } from "../index";
import { directionsApi } from "@/apis";
import { DirectionsResponse } from "@/interaces/directions";

export type LngLat = [number, number];

const actions: ActionTree<MapState, StateInterface> = {
  async getRouteBetweenPoints({ commit }, payload: { origin: LngLat, destination: LngLat }) {
    // a line to prevent linter errors
    const response = await directionsApi.get<DirectionsResponse>(`${payload.origin.join(',')};${payload.destination.join(',')}`);
   
    if (response.data.routes[0]) {
      commit('setDistanceDuration', {
        distance: response.data.routes[0].distance,
        duration: response.data.routes[0].duration
      });
      
      commit('setRoute', response.data.routes[0].geometry.coordinates);
    }
    else {
      console.log('No route found');
    }
  },
};

export default actions;
