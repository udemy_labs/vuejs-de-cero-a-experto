import { MutationTree } from "vuex";
import { MapState } from "./state";
import Mapboxgl from 'mapbox-gl';
import { Feature } from '../../interaces/places';

const mutation: MutationTree<MapState> = {
  setMap( state, map: Mapboxgl.Map ) {
    state.map = map;
  },

  setPlaceMarkers(state, places: Feature[]) { 
  
    // Clear existing markers
    state.markers.forEach(marker => marker.remove());
    state.markers = [];

      if (!state.map) {
      return;
    }
    
    // Add new markers
    places.forEach(place => {
      const [lng, lat] = place.center as [number, number];
     
       const popup = new Mapboxgl.Popup({ offset: 25 })
                .setLngLat([lng, lat])
                .setHTML(`<h5>${ place.text }</h5> <p>${ place.place_name }</p>`)

        const marker = new Mapboxgl.Marker()
            .setLngLat([lng, lat])
            .setPopup(popup)
        .addTo(state.map as Mapboxgl.Map); 
      
        state.markers.push(marker);


    });

    if (state.map?.getLayer('routeString')) {
      state.map?.removeLayer('routeString');
      state.map?.removeSource('routeString');
      state.distance = 0;
      state.duration = 0;
    }
    
  },
  
  setRoute(state, route: number[][]) {
    const origin = route[0];
    const destination = route[route.length - 1]; 

    //definir los bounds
    const bounds = new Mapboxgl.LngLatBounds(
      [origin[0], origin[1]],
      [origin[0], origin[1]],
    );

    //Agregaos cada punto al bounds
    for (const point of route) {
      const newPoint: [number, number] = [point[0], point[1]];
      bounds.extend(newPoint);
    }

    state.map?.fitBounds(bounds, { padding: 300 });

    const sourceData: Mapboxgl.AnySourceData = {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'LineString',
              coordinates: route,
            },
          }
        ],
      },
    }

    if (state.map?.getLayer('routeString')) {
      state.map?.removeLayer('routeString');
      state.map?.removeSource('routeString');
    }


    state.map?.addSource('routeString', sourceData);

    state.map?.addLayer({
      id: 'routeString',
      type: 'line',
      source: 'routeString',
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
      },
      paint: {
        'line-color': '#3887be',
        'line-width': 5,
        'line-opacity': 0.75,
      },
    });
   
  },

  setDistanceDuration(state, payload: { distance: number, duration: number }) {
    let kms = payload.distance / 1000;
    kms = Math.round(kms * 100) / 100;
    
    state.distance = kms;
    state.duration = Math.floor(payload.duration/60);
  },
};

export default mutation;
