import { computed } from "vue";
import { StateInterface } from "@/store";
import { useStore } from "vuex";
import Mapboxgl from 'mapbox-gl';
import { Feature } from "@/interaces/places";
import { LngLat } from "@/store/maps/actions";


export const useMapStore = () => {
    const store = useStore<StateInterface>();

    return {
        map: computed(() => store.state.map.map),
        distance: computed(() => store.state.map.distance),
        duration: computed(() => store.state.map.duration),
        
        // Getters
        isMapReady: computed(() => store.getters['map/isMapReady']),
        //Mutations
        setMap: (map: Mapboxgl.Map) => store.commit('map/setMap', map),
        setPlaceMarkers: (places: Feature[]) => store.commit('map/setPlaceMarkers', places),
        //Actions
        getRouteBetweenPoints: (payload: { origin: LngLat, destination: LngLat }) => store.dispatch('map/getRouteBetweenPoints', payload),
    }
}